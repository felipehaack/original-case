package com.afkl.exercises.spring.common;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.ExportMetricWriter;
import org.springframework.boot.actuate.metrics.CounterService;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.boot.actuate.metrics.buffer.CounterBuffers;
import org.springframework.boot.actuate.metrics.buffer.GaugeBuffer;
import org.springframework.boot.actuate.metrics.buffer.GaugeBuffers;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Predicate;

@Aspect
@Component
@ExportMetricWriter
public class MeasureControllerAOP {

    private static final String MINIMUM = "controller.request.invoked.minimum";
    private static final String MAXIMUM = "controller.request.invoked.maximum";
    private static final String AVERAGE = "controller.request.invoked.average";
    private static final String COUNT = "controller.request.invoked.count";
    private static final String TOTAL = "controller.request.invoked.total";

    private final CounterService counterService;
    private final CounterBuffers counterBuffers;

    private final GaugeService gaugeService;
    private final GaugeBuffers gaugeBuffers;

    @Autowired
    public MeasureControllerAOP(CounterService counterService, CounterBuffers counterBuffers, GaugeService gaugeService, GaugeBuffers gaugeBuffers) {
        this.counterService = counterService;
        this.counterBuffers = counterBuffers;
        this.gaugeService = gaugeService;
        this.gaugeBuffers = gaugeBuffers;
    }

    @Around("within(com.afkl.exercises.spring.controllers..*) && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object result = joinPoint.proceed();
        long elapsedTime = System.currentTimeMillis() - start;

        GaugeBuffer min = gaugeBuffers.find("gauge." + MINIMUM);
        if (min == null || elapsedTime < min.getValue().longValue())
            gaugeService.submit(MINIMUM, elapsedTime);

        GaugeBuffer max = gaugeBuffers.find("gauge." + MAXIMUM);
        if (max == null || elapsedTime > max.getValue().longValue())
            gaugeService.submit(MAXIMUM, elapsedTime);

        Double totalGauge = Optional.ofNullable(gaugeBuffers.find("gauge." + TOTAL))
                .map(GaugeBuffer::getValue)
                .orElse(0D);
        Double total = totalGauge + elapsedTime;
        gaugeService.submit(TOTAL, total);

        Long count = Optional.ofNullable(counterBuffers.find("counter." + COUNT))
                .map(c -> c.getValue() + 1)
                .orElse(1L);
        counterService.increment(COUNT);

        gaugeService.submit(AVERAGE, total / count);

        return result;
    }
}
