package com.afkl.exercises.spring.controllers;

import com.afkl.exercises.spring.fares.Currency;
import com.afkl.exercises.spring.fares.Fare;
import com.afkl.exercises.spring.locations.AirportRepository;
import com.afkl.exercises.spring.locations.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static java.math.RoundingMode.HALF_UP;
import static java.util.Locale.ENGLISH;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/fares/{origin}/{destination}")
public class FareController {

    private final AirportRepository repository;

    private final ExecutorService executor = Executors.newWorkStealingPool();

    @Autowired
    public FareController(AirportRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = GET)
    public Callable<Fare> calculateFare(@PathVariable("origin") String origin,
                                        @PathVariable("destination") String destination,
                                        @RequestParam(value = "currency", defaultValue = "EUR") String currency) {
        return () -> {
            List<Callable<Object>> callables = Arrays.asList(
                    () -> {
                        Thread.sleep(ThreadLocalRandom.current().nextInt(1000, 6000));
                        return 1;
                    },
                    () -> repository.get(ENGLISH, origin).orElseThrow(IllegalArgumentException::new),
                    () -> repository.get(ENGLISH, destination).orElseThrow(IllegalArgumentException::new),
                    () -> new BigDecimal(ThreadLocalRandom.current().nextDouble(100, 3500))
                            .setScale(2, HALF_UP)
            );

            List<Object> parallelTasks = executor.invokeAll(callables).stream().map(f -> {
                try {
                    return f.get();
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            }).collect(Collectors.toList());

            Location o = (Location) parallelTasks.get(1);
            Location d = (Location) parallelTasks.get(2);
            BigDecimal fare = (BigDecimal) parallelTasks.get(3);

            return new Fare(fare.doubleValue(), Currency.valueOf(currency.toUpperCase()), o.getCode(), d.getCode());
        };
    }

}
