## KLM Airlines

### How to Setup
1. Download [Docker For Mac](https://docs.docker.com/docker-for-mac/) on your machine.
1. Install the Docker Application and make sure you have the latest version installed.
1. For other Operating systems, seek more information at [Docker Install](https://docs.docker.com/compose/install/).

### Backend
1. Go to the application folder ```cd original-case/klm-api```

#### How to Run
1. Run the following command: ```docker-compose up```
    - **The first launch of the API takes considerably more time than the others containers** due to the fact that it has to download dependencies, even when Postgres and Redis has finished, the API will continue downloading. Also all tests are executed on the process to build.
2. The API will be running in the 9000 port.

### Frontend
1. Go to the application folder ```cd original-case/klm-web```

#### How to Run
1. Run the following command: ```docker-compose up```
1. Once the application has been launched successfully, go to [localhost:8000](http://localhost:8000)

### Improvements
1. Unfortunately I was not able to finish the extra tasks
1. Improve OAP reports in a understandable way