export const environment = {
    production: false,
    URL: 'http://local.kml.com:9000',
    grantType: 'client_credentials',
    urlAccessToken: '/oauth/token',
    clientId: 'travel-api-client',
    clientSecret: 'psw'
};
