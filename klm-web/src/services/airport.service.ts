import {Injectable} from '@angular/core'
import {RESTService} from './rest.service'
import {Observable} from 'rxjs'
import {Response} from '@angular/http'

@Injectable()
export class AirportService {

  constructor(private restService: RESTService) {
  }

  getAll(): Observable<Response> {
    return this.restService.get('/airports')
  }
}