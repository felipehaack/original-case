import {Observable} from 'rxjs'
import {Injectable} from '@angular/core'
import {Headers, URLSearchParams} from '@angular/http'
import {BaseRequestOptions, RequestOptions, Http, Response} from '@angular/http'
import {environment} from 'environments/environment'
import {RequestOptionsArgs} from '@angular/http/src/interfaces';

@Injectable()
export class RESTService extends BaseRequestOptions {

    private baseUrl: string = environment.URL
    private token: string

    constructor(protected http: Http) {
        super()
    }

    private requestToken(): Observable<Response> {
        const params = new URLSearchParams();
        params.append('grant_type', environment.grantType)
        params.append('client_id', environment.clientId)
        params.append('client_secret', environment.clientSecret)

        const headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')

        const requestOptionArgs: RequestOptionsArgs = {
            headers: headers
        }

        const options = new RequestOptions(requestOptionArgs)

        return this.http.post(`${environment.URL}${environment.urlAccessToken}`, params.toString(), options)
    }

    private getToken(): Observable<string> {
        return new Observable(observable => {
            if (!this.token) {
                this.requestToken().subscribe(
                    (response: Response) => {
                        this.token = response.json().access_token
                        observable.next(this.token)
                    }
                )
            } else {
                observable.next(this.token)
            }
        })
    }

    private getTokenBearer(token: string): RequestOptions {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json')
        headers.append('Authorization', `Bearer ${this.token}`)

        const requestOptionArgs: RequestOptionsArgs = {
            headers: headers
        }

        return new RequestOptions(requestOptionArgs)
    }

    get(url: string, options?: RequestOptions): Observable<Response> {
        return this.getToken().flatMap(
            (token: string) => {
                const o = this.getTokenBearer(token)
                return this.http.get(this.baseUrl + url, o)
            }
        )
    }

    post(url: string, body: any, options?: RequestOptions): Observable<Response> {
        return this.getToken().flatMap(
            (token: string) => {
                return this.http.post(this.baseUrl + url, body, options)
            }
        )
    }
}
