export interface Airport {
    name: string
    code: string
    description: string
}