import {Airport} from './airport.model'

export interface SearchFlight {
  origin: Airport
  destination: Airport
}