import {Component, EventEmitter, Output} from '@angular/core'
import {AirportService} from 'services/airport.service'
import {FormControl, ValidatorFn, Validators} from '@angular/forms'
import {Observable} from 'rxjs/Observable'
import {Airport} from 'models/airport.model'
import {SearchFlight} from 'models/search.model'
import {Response} from '@angular/http'
import 'rxjs/add/operator/startWith'
import 'rxjs/add/operator/map'

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.css']
})
export class SearchComponent {

    @Output() searchAvailableFlight = new EventEmitter<SearchFlight>()

    airports: Airport[] = []

    filteredOriginAirports: Observable<Airport[]>
    filteredDestinationAirports: Observable<Airport[]>

    airportOriginCtrl: FormControl
    airportDestinationCtrl: FormControl

    originAirport: Airport
    destinationAirport: Airport

    constructor(private airportService: AirportService) {

        this.airportOriginCtrl = new FormControl(this.originAirport, [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(3),
            this.isValidOrigin()
        ]);

        this.airportDestinationCtrl = new FormControl(this.destinationAirport, [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(3),
            this.isValidDestination()
        ])

        airportService.getAll()
            .subscribe(
                (response: Response) => {
                    this.airports = response.json()._embedded.locations
                    this.initializeValidator()
                }
            )
    }

    initializeValidator() {

        this.filteredOriginAirports = this.airportOriginCtrl.valueChanges
            .startWith(null)
            .map(state => this.filterStates(state));

        this.filteredDestinationAirports = this.airportDestinationCtrl.valueChanges
            .startWith(null)
            .map(state => this.filterStates(state));
    }

    filterStates(state: string) {
        if (state) {
            return this.airports.filter(airport => {
                const stateLower = state.toLowerCase()
                return (
                    airport.name.toLowerCase().indexOf(stateLower) === 0 ||
                    airport.code.toLowerCase().indexOf(stateLower) === 0 ||
                    airport.description.toLowerCase().indexOf(stateLower) === 0
                )
            });
        }

        return this.airports
    }

    originWasChanged(airport: Airport) {
        this.originAirport = airport
    }

    destinationWasChanged(airport: Airport) {
        this.destinationAirport = airport
    }

    isValidOrigin(): ValidatorFn {
        return (control: FormControl): string[] => {
            return this.originAirport ? null : ['Invalid origin']
        }
    }

    isValidDestination(): ValidatorFn {
        return (control: FormControl): string[] => {
            return this.destinationAirport ? null : ['Invalid destination']
        }
    }

    submit() {
        const searchFlight: SearchFlight = {
            origin: this.originAirport,
            destination: this.destinationAirport
        }
    }
}
